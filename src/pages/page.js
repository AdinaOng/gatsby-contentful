import React from 'react'
import Helmet from 'react-helmet'
import get from 'lodash/get'
import Img from 'gatsby-image'
import Hero from '../components/hero'

class csodPageTemplate extends React.Component {
    render() {
        const csodPage = get(this.props, 'data.contentfulPage')
        const [sectionStyles] = get(this.props, 'data.allContentfulComponentSectionStyles.edges')
        const [pageSections] = get(this.props, 'data.allContentfulPageSection.edges')
        const sectionTitle = [pageSections.node].map(section => section.sectionTitle)

        let sectionData = [sectionStyles.node, pageSections.node]

        console.log(sectionData)

        return (
            <div className = "wrapper">
                <Helmet title={`${csodPage.title}`} />
                <h1 className = "section-headline"> {csodPage.title}</h1>
                <h2> {sectionTitle} </h2>
                <Hero data={sectionStyles.node} />
            </div>

        )
    }
}

export default csodPageTemplate

export const pageQuery = graphql`
  query CsodPage {
    contentfulPage {
      title
    },
    allContentfulPageSection {
      edges {
        node {
          sectionTitle
         }
      }
    },
    allContentfulComponentSectionStyles {
      edges {
        node {
          sectionBackgroundColor
          heroImage {
            sizes(maxWidth: 350, maxHeight: 196, resizingBehavior: SCALE) {
             ...GatsbyContentfulSizes_withWebp
            }
          }
        }
      }
    }
  }
`